package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Node implements NodeInterface {

    //the html document
    private final String document;
    //the node and the string inside it
    private final String subDocument;
    //array containing all the nodes inside the node
    private final List<Node> children = new ArrayList<>();
    //the string inside the node
    private final String inside;
    //the map containing the keys and the values of the node
    private final HashMap<String, String> attributes = new HashMap<>();

    //constructor
    public Node(String document, String subDocument) {
        this.document = document;
        this.subDocument = subDocument;
        inside = fillInside();
        fillChildren();
        fillMap();
    }

    //returns the string inside the node
    @Override
    public String getStringInside() {
        return inside;
    }

    //returns all the nodes in the current node
    @Override
    public List<Node> getChildren() {
        return children;
    }

    //returns the value of a key in the node
    @Override
    public String getAttributeValue(String key) {
        return attributes.get(key);
    }

    //fills inside the node from the subdocument text
    private String fillInside() {
        Pattern pattern = Pattern.compile("<([\\s|\\S]*?)>");
        Matcher matcher = pattern.matcher(subDocument);
        matcher.find();
        String tag = matcher.group(1);

        if (tag.charAt(0) == '/')
            return null;

        else {
            String firstWord = tag.contains(" ") ? tag.split(" ")[0] : tag;
            pattern = Pattern.compile("<" + tag + ">([\\s|\\S]*?)</" + firstWord + ">");
            matcher = pattern.matcher(subDocument);
            matcher.find();
            return matcher.group(1);
        }
    }

    //fills the key value map of the node
    private void fillMap() {
        Pattern pattern = Pattern.compile("<([\\s|\\S]*?)>");
        Matcher matcher = pattern.matcher(subDocument);
        matcher.find();
        String tag = matcher.group(1);
        Pattern pattern1 = Pattern.compile("\\s([\\s|\\S]*?)=");
        Matcher matcher1 = pattern1.matcher(tag);
        while (matcher1.find()) {
            String key = matcher1.group(1);
            Pattern pattern2 = Pattern.compile(key + "=\"([\\s|\\S]*?)\"");
            Matcher matcher2 = pattern2.matcher(tag);
            matcher2.find();
            String value = matcher2.group(1);
            attributes.put(key, value);
        }
    }

    //fill the array containing the children nodes
    private void fillChildren() {
        Pattern pattern = Pattern.compile("<([\\s|\\S]*?)>");
        Matcher matcher = pattern.matcher(subDocument);
        matcher.find();

        while (matcher.find()) {
            String tag = matcher.group(1);
            if (tag.charAt(0) == '/') {

                String tag1 = new StringBuilder(tag).deleteCharAt(0).toString();
                String tag1Word = tag1.contains(" ") ? tag1.split(" ")[0] : tag1;
                Pattern p = Pattern.compile("<" + tag1 + "[\\s|\\S]*>([\\s|\\S]*?)</" + tag1Word + ">");
                Matcher m = p.matcher(document);

                if (m.find())
                    continue;
                else {
                    Pattern p1 = Pattern.compile(matcher.group(0) + "([\\s|\\S]*?)");
                    Matcher m1 = p1.matcher(subDocument);
                    m1.find();
                    children.add(new Node(document, m1.group(0)));
                }
            } else {

                String firstWord = tag.contains(" ") ? tag.split(" ")[0] : tag;
                Pattern pattern1 = Pattern.compile("<" + tag + ">([\\s|\\S]*?)</" + firstWord + ">");
                Matcher matcher1 = pattern1.matcher(subDocument);
                matcher1.find();
                if (children.size() > 0) {
                    if (children.get(children.size() - 1).subDocument.equals(matcher1.group(0)))
                        matcher1.find();
                }
                children.add(new Node(document, matcher1.group(0)));

            }
        }
    }
}