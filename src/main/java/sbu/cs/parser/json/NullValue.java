package sbu.cs.parser.json;

public class NullValue extends Value {

    public NullValue(String value) {
        super(value);
    }

    // get the null value
    public String getNull() {
        return null;
    }

}
