package sbu.cs.parser.json;


public class BoolValue extends Value {

    // the boolean value
    private final Boolean value;

    // constructor
    public BoolValue(String value) {
        super(value);
        this.value = Boolean.valueOf(value);
    }

    // get the boolean value
    public Boolean getBool() {
        return value;
    }

}
