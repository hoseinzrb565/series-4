package sbu.cs.parser.json;


public abstract class Value {

    //the string representation of the value
    protected String strRep;

    //constructor
    public Value(String value) {
        strRep = value;
    }

    public String toString() {
        return strRep;
    }

    //returns the value object
    public Value getValue() {
        return this;
    }

}
