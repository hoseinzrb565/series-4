package sbu.cs.parser.json;


public class ArrayValue extends Value {

    // the array value
    private final int[] value;

    // constructor
    public ArrayValue(String value) {
        super(value);
        this.value = strToArray(value);
    }


    public String toString() {
        return strRep;
    }

    //returns the array value
    public int[] getArray() {
        return value;
    }

    // convert the string containing the array to the actual array
    private int[] strToArray(String arr) {
        String[] items = arr.replaceAll("\\[", "").replaceAll("]",
                "").replaceAll("\\s", "").split(",");
        int[] intArr = new int[items.length];

        for (int i = 0; i < items.length; i++) {
            intArr[i] = Integer.parseInt(items[i]);
        }

        return intArr;
    }


}
