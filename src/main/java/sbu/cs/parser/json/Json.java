package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Json extends Value implements JsonInterface {

    // the string to be parsed
    private final String data;
    // an array of keys contained in the data
    private final ArrayList<Key> keys;

    // constructor
    public Json(String data) {
        super(data);
        this.data = data;
        keys = fillKeys();
    }

    @Override
    //returns the value of the key as a string
    public String getStringValue(String key) {
        return getValue(key).toString();
    }

    // returns the value of a key
    public Value getValue(String key) {
        for (int i = 0; i < keys.size(); i++) {
            if (keys.get(i).getName().equals(key))
                return keys.get(i).getValue();
        }
        return null;
    }

    // fills the keys array
    private ArrayList<Key> fillKeys() {
        ArrayList<Key> keys = new ArrayList<>();
        Pattern pattern = Pattern.compile("\"([^\"]*?)\":", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            String name = matcher.group(1);
            Value value = findValue(name);
            keys.add(new Key(name, value));
        }
        return keys;
    }

    // finds the value of a key
    private Value findValue(String key) {
        key += "\":";
        Pattern pattern = Pattern.compile(key + "([\\s|\\S]*?)(}|,)", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(data);
        matcher.find();
        String str = matcher.group(1);
        if (str.contains("{")) {
            pattern = Pattern.compile(key + "([\\s|\\S]*?)(})", Pattern.DOTALL);
            matcher = pattern.matcher(data);
            matcher.find();
            str = matcher.group(1) + "}";
        } else if (str.contains("[")) {
            pattern = Pattern.compile(key + "([\\s|\\S]*?)(])", Pattern.DOTALL);
            matcher = pattern.matcher(data);
            matcher.find();
            str = matcher.group(1) + "]";
        }

        return strToValue(str.replaceAll("\"", "").trim());
    }

    // converts the string representation of the value to the actual type
    private Value strToValue(String str) {
        // the value is an array
        if (str.charAt(0) == '[' && str.charAt(str.length() - 1) == ']') {
            return new ArrayValue(str);
        }

        // the value is a JSON object
        else if (str.charAt(0) == '{' && str.charAt(str.length() - 1) == '}') {
            return new Json(str);
        }

        // the value is a boolean
        else if (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("false")) {
            return new BoolValue(str);
        }

        // the value is null
        else if (str.equalsIgnoreCase("null") || str == null) {
            return new NullValue(str);
        }

        // the value is a number
        else if (isNumber(str)) {
            return new NumberValue(str);
        }

        // the value is a name
        else {
            return new StringValue(str);
        }
    }

    private Boolean isNumber(String str) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        return pattern.matcher(str).matches();
    }

}
