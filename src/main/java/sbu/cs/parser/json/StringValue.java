package sbu.cs.parser.json;


public class StringValue extends Value {

    // the string value
    private final String value;

    // constructor
    public StringValue(String value) {
        super(value);
        this.value = value;
    }

    // get the string value
    public String getString() {
        return value;
    }

}
