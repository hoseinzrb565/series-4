package sbu.cs.parser.json;


public class Key {

    // name of the key
    private final String name;
    // value of the key
    private final Value value;

    // constructor
    Key(String name, Value value) {
        this.name = name;
        this.value = value;
    }

    // returns the value of the key
    public Value getValue() {
        return value;
    }

    // returns the name of the key
    public String getName() {
        return name;
    }

}
