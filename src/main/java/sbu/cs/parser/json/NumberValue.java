package sbu.cs.parser.json;

import java.math.BigDecimal;

public class NumberValue extends Value {

    // the number value
    private final BigDecimal value;

    // constructor
    public NumberValue(String value) {
        super(value);
        this.value = new BigDecimal(value);
    }

    // get the number value
    public BigDecimal getNumber() {
        return value;
    }

}
